## 联系我们

1. QQ 群 2684913
2. odoojs@outlook.com
3. odoojs 茶馆 负责维护 odoojs 的所有代码
4. 联系 [北京醉桥 微信二维码 ](https://gitee.com/odoowww/odoojs/raw/master/docs/image/zuiqiao.jpg). 添加微信时, 请注明 申请加入 odoojs 茶馆
5. 联系 [CHIFA 微信二维码](https://gitee.com/odoowww/odoojs/raw/master/docs/image/chifa.jpg). 添加微信时, 请注明 申请加入 odoojs 茶馆
6. 参加 odoojs 茶馆活动. 请付茶水费.多少随意. 一杯清茶, 共同研讨 odoojs.

## odoojs 现状 2024-1-6

1. odoojs-rpc 支持 odoo17. 并向下兼容 odoo16. 经测试 支持 odoo13
2. odoojs-api 功能调整为 addons 配置. 包括 menu, action, view
3. 以下 odoojs-ui [源码](https://gitee.com/odoowww/odoojs-ui)
4. odoojs-vue 是 odoojs 逻辑组件库
5. odoojs-react 计划中. 时间不定.
6. odoojs-antdv 基于 antdv 的 odoojs 物理组件库
7. odoojs-uview-plus 基于 uview-plus 的 odoojs 物理组件库
8. addons_odoo 官方 odoo 常用模块的 action view 配置
9. addons_menu_odoo 官方 odoo 常用模块的 menu 配置
10. 以上 odoojs-ui
11. 本仓库下的 demo-project 文件夹, 提供两个 demo. 分别是 pc 端和移动端.
12. 在 [bilibili](https://www.bilibili.com/) 搜索 "醉桥 odoo odoojs" 可获得相关的视频

## demo project

1. 以下几个 demo, 演示如何使用 odoojs
2. [PC 端 Demo](https://gitee.com/odoowww/odoojs-demo-vue-antd) pc 端 demo, 使用 antdv ui 库.
3. [移动端 Demo](https://gitee.com/odoowww/odoojs-demo-vue-uni-uview-plus) 移动端 demo, 使用 uview-plus ui 库.
4. 以下两个测试脚本, 演示如何调用 odoojs-rpc 及 odoojs-api
5. [文件夹 /demo-project/test-odoojs-rpc](https://gitee.com/odoowww/odoojs/tree/master/demo-project/test-odoojs-rpc)
6. [文件夹 /demo-project/test-odoojs-api](https://gitee.com/odoowww/odoojs/tree/master/demo-project/test-odoojs-api)

## odoojs 介绍

### 概述

1. odoojs 是一个产品
2. odoojs 支持 自定义配置
3. odoojs 是一个开发平台
4. odoojs 支持 DIY ERP, 需求侧自主做 ERP.

### odoojs 是一个产品

1. odoojs 是一个产品. 重现 官方 odoo 的所有业务功能
2. 第一阶段, 包括 销售, 采购, 库存, 开票 几个模块. 第二阶段, 将包括 官方 odoo 的所有模块功能
3. odoojs 产品的服务端, 就是 官方 odoo 社区版. 因此 odoojs 产品, 功能 等同于 官方 odoo.
4. odoojs 产品的前端是 重新设计开发的. 因此 odoojs 产品, 功能不局限于官方 odoo.
5. odoojs 产品, 在中国本地化方面, 做出更新.
6. odoojs 产品, 将在报表, 可视化图表, 移动应用上, 进行增强.

### odoojs 支持 自定义配置.

1. odoojs 将 menu, action, view 的定义从官方 odoo 中剥离出来
2. 我们将 官方 odoo 服务端, 命名为 业务服务.
3. 将剥离的 menu, action, view, 命名为 config 服务.
4. config 服务, 支持可视化配置.
5. 通过更新 config 服务. odoojs 产品将适配个性化应用.
6. odoojs 的 config 服务, 支持自定义扩展 menu, action, view.
7. 基于官方 odoo 在业务服务上做的任何二次开发.
8. 业务服务, python 代码模块完成后. odoo 的开发工作结束.
9. 无需, 做 各种 xml 文件的开发.
10. 直接在 config 服务上 可视化定义即可.
11. 从而实现, 自由的 前端组织结构

### odoojs 是一个开发平台

1. odoojs 与官方 odoo 一样, 不仅仅是产品, 更是一个开发平台
2. odoojs 的技术架构. 非常容易做二次开发.
3. odoojs 的业务服务, 仍然是 官方 odoo.
4. odoojs 的 config 服务, 支持自定义扩展 menu, action, view.
5. odoojs 的前端架构. 包括 接口层, config 层, 逻辑组件层, 物理组件层
6. odoojs-rpc 提供 接口层的功能, 提供访问业务服务的若干接口函数.
7. odoojs-api 提供 config 管理功能, 用于定义 menu, action, view
8. odoojs-vue 及 odoojs-react 是逻辑组件库.
9. odoojs-antdv 及 odoojs-uview-plus 是物理组件库
10. 逻辑组件, 完成复杂的组件处理逻辑. 实现模版结构的解析处理.
11. 物理组件, 基于逻辑组件实现. 将 ui 开发以非常友好的方式完全开放给前端工程师.
12. 既有前端开发的充分自由, 又无复杂的逻辑处理(都在逻辑组件里做好了).

### odoojs 支持 DIY ERP, 需求侧自主做 ERP.

1. 如果对上述的自由度还嫌不够. 那么 odoojs 提供自定义一切的基础.
2. odoojs 的业务服务就是官方 odoo. 官方 odoo 做二开, 其高效便捷无需多说.
3. odoojs 实现前后端分离. 前端一切自己发挥
4. odoojs-rpc, odoojs-api 提供基础的数据接口
5. odoojs-vue, odoojs-react, 提供逻辑组件. 可选择, 使用
6. 剩下的, 一切 DIY. Do It yourself. Free Web.

## odoojs 社区发展

1. odoojs 社区, 初期以 odoojs 前后端分离为主题.
2. 逐步发展, 现在推广 odoojs.
3. 诚邀一切对 odoojs 感兴趣的开发者, 客户, 投资者, 共同发展 odoojs

## odoojs 发展历史

1. 第一阶段. 独立搭建 web 服务, 与 odoo 通过 xmlrpc 通讯. 自定义 ui 界面.
2. 第二阶段. 使用 odoo 已有的 web 服务. 扩展 对外接口. 自定义 ui 界面.
3. 第三阶段. 使用 odoo 已有的 web 服务. 使用 odoo 已有对外 jsonrpc 接口.  
   前端获取 odoo 的所有 menu/action/view, 进行解析渲染.
4. 第四阶段. 扬弃官方 odoo 在服务端定义 menu/action/view 的设计思路. 在前端定义所有的 menu/action/view.  
   官方的 menu/action/view, 与业务数据的耦合性太高. 不符合前后端分离的设计思想.  
   在前端定义 menu/action/view. 这样, 官方 odoo 成为一个独立的业务数据服务.  
   而前端, 完全自主设计一切 ui/ue 要素.  
   这个阶段, 前后端分离解决方案, 正式形成. 并命名为 odoojs 前后端分离架构.
5. 第五阶段. 整理前端代码. 形成 odoojs-rpc, odoojs-api 两个标准模块. 并标准化前端 demo 项目的结构.  
   odoojs-rpc 负责与 odoo 的数据接口交互, 以及编辑页面临时数据的暂存.  
   odoojs-api 负责管理所有的 menu/action/view, 多语言翻译. 以及 addons 的管理.  
   前端 demo 项目中, 形成适配 odoo 的标准组件.
6. 第六阶段, 抽象出 odoojs 逻辑组件, 物理组件.  
   odoojs-vue 是 vue 架构的逻辑组件库  
   odoojs-react 是 react 架构的逻辑组件库  
   odoojs-antdv 是 odoojs 基于 antdv 的物理组件库  
   odoojs-uview-plus 是 odoojs 基于 uview-plus 的 物理组件库  
   至此, odoojs 形成产品.

## odoojs 技术原理

### 技术路线

1. odoo 官方源码做服务端
2. odoojs 实现前后端分离
3. odoojs-vue. vue 架构的逻辑组件库
4. odoojs-react. react 架构的逻辑组件库.
5. odoojs-antdv. odoojs 基于 antdv ui 库的逻辑组件库
6. odoojs-uview-plus. odoojs 基于 uview-plus ui 库的逻辑组件库
