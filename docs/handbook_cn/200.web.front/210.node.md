## node 安装

1. [node 官网地址](https://nodejs.org/)

### 测试 node

1. 在命令行方式下, 运行. 查看 node 版本
2. 在命令行方式下, 运行. 查看 npm 版本

```
node --version
npm --version
```

### 安装 yarn, tyarn

1. yarn 是 js 包管理工具
2. yarn 替代 npm 的功能
3. tyarn 替代 yarn 使用国内镜像

```
npm install yarn tyarn -g

```

### 测试 yarn, tyarn

```
yarn --version
tyarn --version
```
