## 登录

1. 参考 odoojs-rpc 文档
2. 将其中的 rpc 改为 api 即可
3. api.login 在执行 rpc.login 的基础上, 额外多获得一些信息,
   供 odoojs-api 中使用:
   当前用户所有在权限组. 当前服务端已安装的模块.

```
import api from '@/odoojs/index.js'

function test_login(){
  const databases = await api.web.database.list()
  console.log(databases)
  const db = 'your_db_name'
  const login = 'your_user_name'
  const password = 'your_password'
  const info = await api.login({ db, login, password })
  const info2 = api.web.session.session_info
}
```

### 检查登录状态

1. 与 登录一样. api.session_check 比 rpc.session_check, 额外多获得一些信息

```
import api from '@/odoojs/index.js'

async function test_session_check(){
  try {
    const res = await api.session_check()
  } catch (error) {
    alert(error)
    // 可以跳转到登录页面
  }
}
```

### 登出

1. 登出接口, api.logout 与 rpc.logout 没有区别.

```
import api from '@/odoojs/index.js'
async function test_logout() {
  await api.logout()
}
```
