### 安装

```
tyarn add axios
tyarn add odoojs-rpc
tyarn add odoojs-api
```

1. odoojs-api 依赖 odoojs-rpc
2. 关于 odoojs-rpc 的使用, 参考 odoojs-rpc 的说明文档

### 初始化 odoojs-api

1. 定义文件 /odoojs/index.js 用于初始化 odoojs-api
2. 文件内容为下述代码
3. rpc 是 odoojs-rpc 的 实例化. 参考 odoojs-rpc 的说明文档
4. OdooJSAPI 管理 addons, action, menu, view. 依赖 OdooJSRPC
5. 注 2023-12-18: action, view 功能已经移动到 odoojs-rpc 中. odoojs-api 的代码升级中
6. 变量 addons_dict, 指定所有要加载的 addons 文件
7. addon 开头的几个文件, 定义了 action/view
8. addons_odoo 包含所有官方 odoo 的常见模块的 action, view
9. addons_l10n_zh_CN_odoo 是 addons_odoo 的中文翻译包.
10. 其他语言翻译包可仿照此自行设计. 注意命名格式.
11. addons_menus_odoo 是官方 odoo 常见模块的菜单
12. addons_l10n_zh_CN_menus_odoo 是 addons_menus_odoo 的中文翻译包
13. 自定义的 action, view 可以仿照 addons_odoo 自行设计. 注意遵守命名规范
14. 自定义的菜单 可以仿照 addons_menus_odoo 设计
15. 自定义的 addons 应该放在 参数 addons_dict 中
16. modules_installed 参数. 是对服务端 odoo 已安装模块的进一步过滤条件
17. 初始化之后, 全局可以使用变量 api
18. api.lang 设置前端的默认语言
19. 设置默认语言为中文. 若不设置默认语言, 则为英文 en_US
20. 前端可以补充代码, 从 local storage 或 session storage 中 获取默认语言
21. 实际上 api.rpc 就是这里的 rpc. 可以通过 api.rpc 使用 rpc 中的所有功能
22. 为了使用方便, 做了一些简化处理, 如:
23. api.env 就是 api.rpc.env
24. api.baseURL 就是 api.rpc.baseURL
25. api.web 就是 api.rpc.web

```
import { Request } from 'odoojs-rpc/dist/request_axios'
// import { Request } from 'odoojs-rpc/dist/request_uni_uview'
import { OdooJSRPC } from 'odoojs-rpc'
import { OdooJSAPI } from 'odoojs-api'

import addons_odoo from 'odoojs-api/dist/addons_odoo/index.js'
import addons_l10n_zh_CN_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_odoo/index.js'
import addons_menus_odoo from 'odoojs-api/dist/addons_menus_odoo/index.js'
import addons_l10n_zh_CN_menus_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_menus_odoo/index.js'

function messageError(error) { alert(error.message) }
const baseURL = '/api'
const timeout = 50000
const run_by_server = false
export const rpc = new OdooJSRPC({
  Request, baseURL, timeout, messageError, run_by_server })

const addons_dict = {
  addons_odoo,
  addons_l10n_zh_CN_odoo,
  addons_menus_odoo,
  addons_l10n_zh_CN_menus_odoo
}

const modules_installed = [
  'base',
  'product',
  'analytic',
  'account',
  'contacts',
  'sale',
  'purchase'
]

export const api = new OdooJSAPI(rpc, {addons_dict, modules_installed})
api.lang = 'zh_CN'
export default api
```
