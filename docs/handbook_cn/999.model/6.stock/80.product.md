## product.product

### stock 专用属性

1. stock_quant_ids 库存
2. stock_move_ids 移库记录
3. orderpoint_ids 最小库存规则
4. putaway_rule_ids putaway 规则?
5. storage_category_capacity_ids ?
6.

7. nbr_moves_in 过去 12 个月的 入库记录数
8. nbr_moves_out 过去 12 个月的 出库记录数
9. nbr_reordering_rules 再订购规则数
10. reordering_min_qty 最小库存限制
11. reordering_max_qty 最大库存限制

### 几个数量字段

1. incoming_qty 待入库
2. outgoing_qty 待出库
3. qty_available, 在手数量.
4. virtual_available 预测数量 = 在手 - 出库 + 入库
5. free_qty 可用数量 = 在手 - 保留

### 关于数量字段

1. context 中携带单库位/单仓库
2. 则 其及儿孙所有库位的数量
3. 否则 显示所有内部 库位
4. lot_id / owner_id / package_id / from_date / to_date
5. location / warehouse: list or single, [name or id]
