#### odoo view 探讨

1. odoo 的 view 提供了一种用模板生成 html 页面的机制
2. view 中嵌入了 大量的 html 要素. 导致 view 不是通用的模板, 成为 odoo 专用的
3. view 中嵌入了 python 脚本. 导致 浏览器在渲染时, 需要用 js 解析 python 脚本
4. 市场上, 流行的 前端架构, 无法直接使用 view 进行渲染
