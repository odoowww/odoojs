import { rpc } from '../odoojs/index.js'
import { login } from './login.js'

// const model_name = 'res.partner'
// const fields = {
//   name: {},
//   email: {},
//   bank_ids: { fields: ['bank_bic', 'bank_id'] }
// }

// const model_name = 'res.bank'
// const fields = {
//   name: {},
//   email: {}
// }

const model_name = 'res.partner.category'
const fields = {
  name: {},
  color: {}
}

let editmodel = null

const state = {
  editable: false,
  fields: {},
  record_only: {},
  record_display: {},
  values: {}
}

async function init() {
  const Model = rpc.env.model(model_name)
  // 创建一条测试数据, 用于运行这里的测试代码
  const resId = await Model.create({ name: 'TAG-Test' })
  // 创建数据结束, 后续是 编辑用的代码
  // 实际场景中 resId 通常来自于 页面参数

  const metadata = await Model.load_metadata({ fields })
  state.record_only = await Model.load_data_one(resId, { fields })
  editmodel = rpc.env.editmodel(model_name, { metadata })
  state.record_display = editmodel.set_edit({
    record: state.record_only,
    values: state.values
  })
  state.fields = editmodel.get_fields_all(fields)
  state.editable = true

  console.log('state', state)
}

async function onChange(fname, value) {
  const result = await editmodel.onchange(fname, value)
  const { record_display, values, domain } = result
  state.record_display = { ...record_display }
  state.values = { ...values }
  state.fields = editmodel.get_fields_all(fields)
  console.log('state', state)
}

async function handelCommit() {
  const id_ret = await editmodel.commit((done) => {
    done(true)
  })

  console.log('id_ret', id_ret)
  state.editable = false
  const Model = rpc.env.model(model_name)
  const res = await Model.load_data_one(id_ret, { fields })
  console.log('new ok', res)

  //
  // 这是测试代码中用的, 删除该条临时数据
  await Model.unlink(id_ret)
  //
}

async function test() {
  await login()
  await init()
  await onChange('name', 'TAG-' + Math.random())
  await onChange('color', 2)
  await handelCommit()
}

test()
