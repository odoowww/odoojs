import { rpc } from '../odoojs/index.js'

async function test_db_list() {
  const result = await rpc.web.database.list()
  console.log('db list', result)
}

async function test_login() {
  const db = 'T1'
  const login = 'admin'
  const password = '123456'

  const info = await rpc.login({ db, login, password })
  const info2 = rpc.web.session.session_info
  console.log('login session_info', info2)
}

async function test_session_check() {
  const res = await rpc.session_check()

  if (res) {
    console.log('session check res:', res)
  } else {
    console.log('session error:')
    // 可以跳转到登录页面
  }
}

async function test_logout() {
  const res = await rpc.logout()
  console.log('logout', res)
}

function get_session_id() {
  const sid = rpc.web.session.get_session_id()
  console.log('session_id:', sid)
}

async function test() {
  await test_db_list()
  get_session_id()
  await test_login()
  get_session_id()
  await test_session_check()
  await test_logout()
  get_session_id()
  console.log('test_session_check 1')
  await test_session_check()
  console.log('test_session_check 2')
}

test()
