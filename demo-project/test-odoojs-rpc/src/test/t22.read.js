import { rpc } from '../odoojs/index.js'
import { login } from './login.js'

const model_name = 'res.country'
const fields = {
  name: {},
  code: {},
  state_ids: { fields: ['name', 'code'] }
}

async function test_load_data_one() {
  const res_id = 48
  const Model = rpc.env.model(model_name)

  const context = { lang: 'en_US' }
  const metadata = await Model.load_metadata({ fields })
  const record = await Model.load_data_one(res_id, { fields, context })

  console.log('record', record)
}

async function test() {
  await login()
  await test_load_data_one()
}

test()
