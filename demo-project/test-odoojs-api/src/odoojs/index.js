import { Request } from 'odoojs-rpc/dist/request_axios.js'
import { OdooJSRPC } from 'odoojs-rpc'
import { OdooJSAPI } from 'odoojs-api'

import addons_odoo from 'odoojs-api/dist/addons_odoo/index.js'
import addons_l10n_zh_CN_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_odoo/index.js'
import addons_menus_odoo from 'odoojs-api/dist/addons_menus_odoo/index.js'
import addons_l10n_zh_CN_menus_odoo from 'odoojs-api/dist/addons_l10n_zh_CN_menus_odoo/index.js'

function messageError(error) {
  console.log('show error', error.message)
  return Promise.reject(error)
}

// const baseURL = 'http://127.0.0.1:8069'
// const baseURL = 'http://192.168.56.101:8069' // odoo 16
const baseURL = 'http://192.168.56.103:8069' // odoo 17
const timeout = 50000
const run_by_server = true
export const rpc = new OdooJSRPC({
  Request,
  baseURL,
  timeout,
  // run_by_server,
  messageError
})

const addons_dict = {
  addons_odoo,
  addons_l10n_zh_CN_odoo,
  addons_menus_odoo,
  addons_l10n_zh_CN_menus_odoo
}

const modules_installed = [
  // 'base', 'account'
]

const api = new OdooJSAPI(rpc, { addons_dict, modules_installed })

api.lang = 'zh_CN'

export default api
