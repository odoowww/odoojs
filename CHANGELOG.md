## 分支管理

### master 分支

1. 基于 vue 3.2.x 和 antv 4.x.x
2. odoojs-rpc 升级
3. 更新为 odoojs-rpc odoojs-api 两个依赖库

### odoojs-vue3-rpc-v1 分支

1. 基于 vue 3.2.x 和 antv 3.2.x
2. odoojs-rpc 为旧版本

### odoojs-vue2 分支

1. 基于 vue2 和 antv1.7.8
2. 扬弃 odoo 官方的 menu、action、view,
3. 在 odoojs 前端全部重新定义 menu、action、view
4. odoorpc 代码更新, 优化 menu、action、view 的处理
5. 2023-3-6 创建该分支, 基于原 master 分支创建
6. 2023-3-6 之后, master 另做他用

### odoojs-classic 分支

1. 直接读取 odoo 服务端的 menu、action、view, 在 odoojs 前端呈现
2. 该分支保留了 odoo 官方原汁原味的前端功能
3. 2022-10-10 创建该分支, 基于原 master 分支创建
4. 2022-10-10 之后, master 另做他用

## 更新历史

#### 2023-8-20

1. 创建 odoojs-vue3-rpc-old 分支
2. odoojs-rpc 升级. 旧代码保留于此

#### 2023-3-8

1. 创建 odoojs-vue2 分支
2. odoojs-vue2 分支. 基于 vue2 和 antv1.7.8
3. 更新 master 分支. 基于 vue 3.2.13 和 antv 3.2.15

#### 2022-10-10

1. 基于原有 master 分支, 创建 odoojs-classic 分支
2. odoojs-classic 分支, 直接读取 odoo 服务端的 menu、action、view, 在 odoojs 前端呈现
3. odoojs-classic 分支,保留了 odoo 官方原汁原味的前端功能
4. master 分支, 继续维护最新的代码
5. master 分支, 扬弃 odoo 官方的 menu、action、view, 在 odoojs 前端全部重新定义
6. master 分支的 odoorpc 代码更新, 优化 menu、action、view 的处理

#### 2022-2-13

1. odoorpc 优化
2. odooapi 优化
3. 调整 api.my.home 接口. 从 odoorpc 移动到 odooapi

#### 2022-2-7

1. portal 页面. /my/home
2. 自定义菜单.
3. 自定义 xml

#### 2022-1-25

1. 使用 this.\$route.meta.routes 存路由历史
2. 使用 localStorage 存 routes. 页面手动刷新时, 保留路由历史
3. 页头 title. 数据取自 routes. 显示层级结构
4. action.load 时,仅 load action。 不再直接 load_views, 而在需要时 由页面 load views
5. 附件上传与下载

#### 2022-1-23

odoorpc/odooapi 完善.

1. session info store in request class
2. ir.filters 功能
3. o2m tree view. line edit
4. kanban view. button click event
5. file import api

UI 组件完善

1. pivot view / graph view / calendar view
2. 收藏按钮
3. o2m tree view. line edit
4. kanban view. dropdown menu

#### 2022-1-12

odoorpc/odooapi 代码更新. 纯函数方式
UI 组件完善

#### 2021-12-3

选择 ant-design-vue 做 UI
odoorpc/odoojs 代码优化

#### 2021-7-26

odoojs 重大改进
访问 odoo 的方式更简洁

#### 2020-8-30

之前使用 react 实现的 odoojs
之后 使用 vue
